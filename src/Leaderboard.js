import React from 'react';
import Players from './Players';
import Addpemain from './Addpemain';

class Leaderboard extends React.Component {
    constructor(props) {
        super(props)

        const PLAYERS = [
            {id: 1, name: "Brendan Eich", score: 25},
            {id: 2, name: "Chris Lattner", score: 10},
            {id: 3, name: "Jose Valim", score: 30}
        ]

        this.state = {
            members: PLAYERS
        }

        this.onPlayeradd = this.onPlayeradd.bind(this)
    }
    onPlayeradd(name) {
        let new_members = this.state.members
        new_members.push({
            id: new_members.length + 1, name: name, score: 10
        })
        this.setState({
            members: new_members
        })
    }
    render() {


        const styles = {
            container: {
                padding: 60
            }
        }
        return (
                <div style={styles.container}>
                    <h1>Leaderboard</h1>
                    <Players members={this.state.members}/>
                    <Addpemain onAdd={this.onPlayeradd}/>
                </div>
                )
    }
}

export default Leaderboard;