import React from 'react';
import ReactDom from 'react-dom';

import Leaderboard from './Leaderboard';

ReactDom.render(
        <Leaderboard/>, document.getElementById('root')
        );