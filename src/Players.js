import React from 'react';

import Pemain from './Pemain';
class Players extends React.Component {
    render() {
        return (
                <div>
                    {this.props.members.map((pemain) => {
                                    return(
                                                <Pemain key={pemain.id} name={pemain.name} score={pemain.score}/>
                                            )
                    })}

                </div>
                            )
            }
        }

        export default Players;