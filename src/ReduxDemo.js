import React, {Component}
from 'react';
import { createStore
} from 'redux';

class ReduxDemo extends Component {
    render() {

        const reducer = function (state, action) {
            if (action.type === "ATTACK") {
                return action.payload;
            }
            if (action.type === "GREENATTACK") {
                return action.payload;
            }
            return state;
        }


        const store = createStore(reducer, "Peace");

        store.subscribe(() => {
            console.log(" Store is now", store.getState());
        });

        store.dispatch({type: "ATTACK", payload: "IRON MAN"});
        store.dispatch({type: "GREENATTACK", payload: "HULK"});
        return(
                <div>
                    test
                </div>
                )
    }
}

export default ReduxDemo;
